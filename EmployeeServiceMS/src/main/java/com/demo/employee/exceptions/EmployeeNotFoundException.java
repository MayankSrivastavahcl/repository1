package com.demo.employee.exceptions;

/*
 * @author amol M
 * Designing the global exception handler part
 */

public class EmployeeNotFoundException extends RuntimeException {

	public EmployeeNotFoundException(String msg) {
		super(msg);
	}

}
