package com.demo.employee.exceptions;

/*
 * @author amol M
 * Designing the global exception handler part
 */

public class DataNotFoundException extends RuntimeException {

	public DataNotFoundException(String msg) {
		super(msg);
	}

}
