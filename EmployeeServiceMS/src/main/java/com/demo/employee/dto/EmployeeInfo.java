package com.demo.employee.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mayank Srivastava
 * */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EmployeeInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int employeeId;
	private String employeeName;
	private String employeeOrg;
	private Long employeePhone;
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getEmployeeOrg() {
		return employeeOrg;
	}
	public void setEmployeeOrg(String employeeOrg) {
		this.employeeOrg = employeeOrg;
	}
	public Long getEmployeePhone() {
		return employeePhone;
	}
	public void setEmployeePhone(Long employeePhone) {
		this.employeePhone = employeePhone;
	}
	@Override
	public String toString() {
		return "EmployeeInfo [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeOrg="
				+ employeeOrg + ", employeePhone=" + employeePhone + "]";
	}
}
