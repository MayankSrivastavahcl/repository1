package com.demo.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author Mayank Srivastava
 * 
 **/
@SpringBootApplication
public class StartEmployeeServiceMS implements CommandLineRunner{

static Logger log=LoggerFactory.getLogger(StartEmployeeServiceMS.class);
	
	public static void main(String[] args) {
		log.info(" EmployeeServiceMS - Begin ");
		SpringApplication.run(StartEmployeeServiceMS.class, args);
		log.info(" EmployeeServiceMS - End ");
	}
	@Override
	public void run(String... args) throws Exception {
		log.info(" EmployeeServiceMS - Launched.... ");
		
	}
}
