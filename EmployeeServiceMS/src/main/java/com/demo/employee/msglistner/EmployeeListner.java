package com.demo.employee.msglistner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.employee.MyEmployeeBeanConfig;
/*
 * @author amol M
 * Designing the consumer part
 */
import com.demo.employee.entity.Employee;
import com.demo.employee.sender.service.SenderService;

@Service
public class EmployeeListner {

	static Logger log = LoggerFactory.getLogger(EmployeeListner.class);
	
	@Autowired
	SenderService senderService;

	@RabbitListener(queues = MyEmployeeBeanConfig.EMPLOYEE_QUEUE)
	public void readEmployeeData(Employee employee) {
		log.info("-- EmployeeListner -- readEmployeeData) --");
		System.out.println("Employee Received :" + employee);
		senderService.sendEmployeeData(employee);
		log.info("-- EmployeeListner -- readEmployeeData --");
	}

}
