package com.demo.employee;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author Mayank Srivastava
 * 
 **/
@SpringBootApplication
@EnableSwagger2
public class MyEmployeeBeanConfig implements WebMvcConfigurer {

	public static final String EMPLOYEE_EXCHANGE="employee-exchange";
	public static final String EMPLOYEE_QUEUE="employee-queue";
	
	public static final String CONSUMER_EXCHANGE="consumer-exchange";
	public static final String CONSUMER_QUEUE="consumer-queue";

	@Bean(name="myEmployeeExchange")
	Exchange createEmployeeExchange() {
		return ExchangeBuilder.topicExchange(EMPLOYEE_EXCHANGE).build();
	}
	@Bean(name="myEmployeeQueue")
	Queue createEmployeeQueue() {
		return QueueBuilder.durable(EMPLOYEE_QUEUE).build();
	}
	
	@Bean
	Binding employeeBinding(Queue myEmployeeQueue,TopicExchange myEmployeeExchange) {
		return BindingBuilder.bind(myEmployeeQueue).to(myEmployeeExchange).with(EMPLOYEE_QUEUE);
	}
	
	@Bean(name="myConsumerExchange")
	Exchange createConsumerExchange() {
		return ExchangeBuilder.topicExchange(CONSUMER_EXCHANGE).build();
	}
	@Bean(name="myConsumerQueue")
	Queue createConsumerQueue() {
		return QueueBuilder.durable(CONSUMER_QUEUE).build();
	}
	
	@Bean
	Binding consumerBinding(Queue myConsumerQueue,TopicExchange myConsumerExchange) {
		return BindingBuilder.bind(myConsumerQueue).to(myConsumerExchange).with(CONSUMER_QUEUE);
	}
}
