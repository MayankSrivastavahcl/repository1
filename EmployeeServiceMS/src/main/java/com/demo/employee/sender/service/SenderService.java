package com.demo.employee.sender.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.employee.MyEmployeeBeanConfig;
import com.demo.employee.dto.EmployeeInfo;
import com.demo.employee.entity.Employee;
import com.demo.employee.exceptions.DataNotFoundException;

/**
 * @author Mayank Srivastava
 * 
 * */

@Service
public class SenderService {
	
	static Logger log=LoggerFactory.getLogger(SenderService.class);
			

	@Autowired
	private RabbitTemplate rabbitTemplate;

	@RabbitListener(queues = MyEmployeeBeanConfig.CONSUMER_QUEUE)
	public void sendEmployeeData(Employee empinfo) {
		log.info("--TicketSender--sendTicketMessage()--Begin");
		String routingKey=MyEmployeeBeanConfig.CONSUMER_QUEUE;
		if(empinfo!=null) {
		
			rabbitTemplate.convertAndSend(routingKey,empinfo);
			
		}else {
			throw new DataNotFoundException("Not Received Employee Information");
		}
		log.info("--TicketSender--sendTicketMessage()--End");
	}
}
