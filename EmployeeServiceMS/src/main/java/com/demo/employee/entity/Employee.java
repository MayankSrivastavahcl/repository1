package com.demo.employee.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Mayank Srivastava
 * */

@Entity
@Table(name="myemployee",schema = "myemployeedb")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int employeeId;
	
	@Column(name="name")
	private String employeeName;
	
	@Column(name="org")
	private String employeeOrg;
	
	@Column(name="phone")
	private Long employeePhone;
	
	public Employee() {}

	public Employee(String employeeName, String employeeOrg, Long employeePhone) {
		super();
		this.employeeName = employeeName;
		this.employeeOrg = employeeOrg;
		this.employeePhone = employeePhone;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeOrg() {
		return employeeOrg;
	}

	public void setEmployeeOrg(String employeeOrg) {
		this.employeeOrg = employeeOrg;
	}

	public Long getEmployeePhone() {
		return employeePhone;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeOrg=" + employeeOrg
				+ ", employeePhone=" + employeePhone + "]";
	}

	public void setEmployeePhone(Long employeePhone) {
		this.employeePhone = employeePhone;
	}
	
}
