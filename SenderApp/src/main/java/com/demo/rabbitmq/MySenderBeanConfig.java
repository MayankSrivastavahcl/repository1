package com.demo.rabbitmq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * 
 * @author Mayank Srivastava
 * 
 **/

@SpringBootApplication
@EnableSwagger2
public class MySenderBeanConfig implements WebMvcConfigurer{

	public static final String EMPLOYEE_EXCHANGE="employee-exchange";
	public static final String EMPLOYEE_QUEUE="employee-queue";

	@Bean(name="myEmployeeExchange")
	Exchange createEmployeeExchange() {
		return ExchangeBuilder.topicExchange(EMPLOYEE_EXCHANGE).build();
	}
	@Bean(name="myEmployeeQueue")
	Queue createEmployeeQueue() {
		return QueueBuilder.durable(EMPLOYEE_QUEUE).build();
	}
	
	@Bean
	Binding employeeBinding(Queue myEmployeeQueue,TopicExchange myEmployeeExchange) {
		return BindingBuilder.bind(myEmployeeQueue).to(myEmployeeExchange).with(EMPLOYEE_QUEUE);
	}
	private ApiInfo apiDetails() {
        return new ApiInfo("SenderApp API","SenderApp - part of EmployeeServiceMS","1.1",
                "Free API to use any time",new springfox.documentation.service.Contact("Mayank Srivastava","https://www.myapp.com/our-mode-1-2-3-strategy", "mayank.srivastava@any.com"),
                "API under Fee Licence","https://www.myapp.com", null);
    }
    @Bean
    public Docket api() {
         return new Docket(DocumentationType.SWAGGER_2)
                  .select()                                 
                  .apis(RequestHandlerSelectors.any())             
                    .paths(PathSelectors.any())                         
                  .build();
    }
}
