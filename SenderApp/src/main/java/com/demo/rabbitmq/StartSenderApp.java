package com.demo.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 * 
 * @author Mayank Srivastava
 * 
 **/
public class StartSenderApp implements CommandLineRunner {
	static Logger log=LoggerFactory.getLogger(StartSenderApp.class);
	
	public static void main(String[] args) {
		log.info(" SenderApp - Begin ");
		SpringApplication.run(StartSenderApp.class, args);
		log.info(" SenderApp - End ");
	}
	@Override
	public void run(String... args) throws Exception {
		log.info(" SenderApp - Launched.... ");
		
	}
}
